#define MAX_NUM 255
#define RANDOM_NUMBERS 0

const short OUT_PINS[8] = {5,6, 7, 8, 9, 10, 11, 12};
// PIN 12 - MSB

//const short OUT_PINS[8] = {12,11,10,9,8,7,6,5};
// PIN 6 - MSB

byte currentNum =0;

// true for increment 
bool toogleIncDec = true;


void setup() {
  // put your setup code here, to run once:
  for(char i=0;i<8;i++) {
     pinMode(OUT_PINS[i], OUTPUT);
  }
  Serial.begin(9600);
  randomSeed(analogRead(0));
}

 byte getNumSequence(){


  if(toogleIncDec) currentNum++;
  else currentNum--;

  // Switch increment or dec if number  <0,MAX_NUM>
  if(currentNum == MAX_NUM || currentNum ==0) toogleIncDec=!toogleIncDec;



  return currentNum;

 }
 

void loop() {

  byte b = RANDOM_NUMBERS ? (byte)(random(MAX_NUM) & 0xFF) : getNumSequence(); // random number in range 0 - MAX_NUM inclusive

  Serial.print(b); Serial.print('\t');

  for(char i=7;i>=0;i--){
    digitalWrite(OUT_PINS[i], (b & (1 << i)) != 0);
      Serial.print((b & (1 << i)) != 0 ? 1 : 0);
  }

  //delay(50);
  Serial.println();
  
 
}
