
const short OUT_PINS[8] = {5, 6, 7, 8, 9, 10, 11, 12};

void setup() {
  // put your setup code here, to run once:
  for(char i=0;i<8;i++) {
     pinMode(OUT_PINS[i], OUTPUT);
  }
  
  Serial.begin(9600);
  randomSeed(analogRead(0));
}

void loop() {
  // put your main code here, to run repeatedly:

  byte b = (byte)(random(256) & 0xFF) ; // random number in range 0 - 255 inclusive

  Serial.println(b);

  for(char i=0;i<8;i++){
    digitalWrite(OUT_PINS[i], (b & (1 << i)) != 0);
  }
  
 delay(5000);         
}
